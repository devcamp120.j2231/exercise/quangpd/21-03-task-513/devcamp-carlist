import { gJsonCarsParse } from "./info.js";
import { checkCarOldOrNew } from "./info.js";
function App() {
  return (
    <div>
      <ul>
        {gJsonCarsParse.map((item,index) =>{
          return <li key={index}>Tên : {item.model}, Biển Số :{item.vID} ,Năm Sản Xuất : {item.year} ,Loại : {checkCarOldOrNew(item.year)} </li>
        })}
      </ul>
    </div>
  );
}

export default App;
